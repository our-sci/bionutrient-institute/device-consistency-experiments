---
title: "Device Consistency Testing"
author: "Bionutrient Institute."
output: html_document
---

```{r init, echo=FALSE, warning=FALSE, message=FALSE}

plotAgain <- FALSE

library(tidyverse)
library(Hotelling)
library(DT)
source("./src/plot_and_analysis_scripts.R")

knitr::opts_chunk$set(
                    echo=FALSE,
                    warning=FALSE,
                    message=FALSE
                  )
theme_set(theme_gray(base_size = 18))

baselineData <- read_csv( "./datasets/30-Device Consistency -- Baseline Testing.csv" )
grainData <- read_csv( "./datasets/30-Device Consistency -- Grains Testing.csv" )
soilData <- read_csv( "./datasets/30-Device Consistency -- Soil Testing.csv" )
```

```{r grainData}

grain <- grainData %>%
  mutate(
    deviceID = as.factor( `data.oat_8536.scan_8536_5.value.device_id` ),
  ) %>% select(
          deviceID,
          matches( "data.(wheat|oat)\\_[0-9]{3,4}.scan\\_[0-9]{3,4}\\_[1,2,3,4,5,6]{1}.value.median_[0-9]{3,4}" ),
          ) %>%
  rename_all( ~ str_remove_all( string = . , pattern= "data\\." ) ) %>%
  rename_all( ~ str_remove_all( string = . , pattern= "(\\.value.median)" ) ) %>%
  rename_all( ~ str_remove_all( string = . , pattern= "([0-9]{3,4}\\_)" ) )

grainLong <- grain %>%
  pivot_longer( cols = contains("scan"),
               names_pattern = "(wheat|oat)\\_([0-9]{3,4})\\.scan\\_([1,2,3,4,5,6]{1})_([0-9]{3,4})",
               names_to = c( "species", "sample_id", "repetition", "wavelength" )
               ) %>%
  mutate( material = str_c( species, sample_id, sep = "_" ) ) %>%
  select( - species, - sample_id ) %>%
  na.omit

```


```{r soilData}

soil <- soilData %>%
  mutate(
    deviceID = as.factor( `data.sample_6132.scan_6132_6.value.device_id` ),
    ) %>% select(
            deviceID,
            matches( "data.sample\\_[0-9]{3,4}.scan\\_[0-9]{3,4}\\_[1,2,3,4,5,6]{1}.value.median_[0-9]{3,4}" ),
            ) %>%
  rename_all( ~ str_remove_all( string = . , pattern= "data\\." ) ) %>%
      rename_all( ~ str_remove_all( string = . , pattern= "(\\.value.median)" ) ) %>%
      rename_all( ~ str_remove_all( string = . , pattern= "([0-9]{3,4}\\_)" ) )

soilLong <- soil %>%
  pivot_longer( cols = contains("scan"),
               names_pattern = "sample\\_([0-9]{3,4})\\.scan\\_([1,2,3,4,5,6]{1})_([0-9]{3,4})",
               names_to = c( "material", "repetition", "wavelength" )
               ) %>%
  mutate( material = str_c( "soil", material, sep = "_" ) ) %>%
  na.omit

```



```{r baseline}

## 01:52 has two teflons, first one is failed
## TODO exclude the teflon for everything except the transitional models

calibrations <- baselineData %>%
  mutate(
    deviceID = as.factor( `data.standard.scan_1.value.device_id` ),
    calibrationDevice1 = `data.intro_info.rfc_device.value.0`,
    calibrationDevice2 = `data.intro_info.rfc_device.value`,
    calibrationDevice = ifelse( is.na(calibrationDevice1), calibrationDevice2, calibrationDevice1 ),
    calibrationDevice = ifelse( calibrationDevice == "yes_1", "teflon", "barium" ),
    date = `meta.dateCreated`
  ) %>% select(
          deviceID,
          calibrationDevice,
          - calibrationDevice1,
          - calibrationDevice2,
          matches( "data.standard.scan_[1,2,3,4,5,6]{1}.value.median_[0-9]{3,4}" ),
          matches( "data.cuvettes.((dark)|(medium)|(light))_cuvette.scan_[1,2,3,4,5,6]{1}.value.median_[0-9]{3,4}" ),
          matches( "data.cards.((dark)|(medium)|(light))_card.scan_[1,2,3,4,5,6]{1}.value.median_[0-9]{3,4}" ),
          matches( "data.cards.((dark)|(medium)|(light))_card.scan_[1,2,3,4,5,6]{1}.value.temperature" ),
          date
        ) %>%
  rename_all( ~ str_remove_all( string = . , pattern= "data\\." ) ) %>%
  rename_all( ~ str_remove_all( string = . , pattern= "((cards)|(cuvettes))\\." ) )
## calibrations %>% select( contains("median_500") ) %>% select(contains("scan_1"))  %>% colnames

labDevices <- calibrations %>%
  filter( calibrationDevice == "barium" ) %>%
  group_by( deviceID ) %>%
  dplyr::summarise( n = n() ) %>%
  filter( n == 2 ) %>%
  .$deviceID %>%
  as.character %>%
  unlist 

allDevices <- calibrations$deviceID %>% as.character %>% unique


calibrations <- calibrations %>%
  mutate(
    deviceID = as.character(deviceID),
    deviceID = ifelse( ( deviceID %in% labDevices ) & ( date > "2021-06-28" ), paste0( deviceID, "-2" ), deviceID ),
    deviceID = as.factor(deviceID)
  )

calibrationsLong <- calibrations %>%
  select( - contains("temperature"), - date ) %>%
  pivot_longer( cols = contains("scan"),
               names_pattern = "(standard|light\\_cuvette|medium\\_cuvette|dark\\_cuvette|light\\_card|medium\\_card|dark\\_card).scan_(.).value.median_([0-9]{3,4})",
               names_to = c( "material", "repetition", "wavelength" )
               ) %>%
  filter( calibrationDevice == "barium" ) %>%
  na.omit %>%
  select( - calibrationDevice )

measurementsLong <- bind_rows( calibrationsLong, grainLong, soilLong )

## merging temp
## left_join( x=measurementsLong, y=tempsLong, by=c( 'deviceID', 'material', 'repetition' ) ) %>% filter( !is.na(temperature) )

waveLengths <- c("365", "385", "450", "500", "530", "587", "632", "850", "880", "940")

wideMeasurements <- measurementsLong %>%
  ## group_by( repetition, deviceID, calibrationDevice ) %>%
  pivot_wider( values_from = value, names_from = wavelength, values_fn = list ) %>%
  unnest( cols = !!waveLengths )

materials <- wideMeasurements$material %>% unique %>% as.character
devices <- wideMeasurements$deviceID %>% unique %>% as.character

lightCuvette <- wideMeasurements %>%
  filter( material == "light_cuvette" ) %>%
  select( !!waveLengths, deviceID, repetition )

lightCuvetteTest <- manova( cbind( `365`,`385`, `450`, `500`, `530`, `587`, `632`, `850`, `880` ) ~ deviceID, lightCuvette )


## manovaEx <- lightCuvetteTest %>% summary
## manovaEx$stats %>% as_tibble %>% select( `Pr(>F)` ) %>% .[[1,1]]
## lightCuvetteTest %>% summary.aov

outliersIDs <- c()

allMANOVATests <- tibble(
  material = materials,
  map_df( materials, ~ applyMANOVAReps( materialName = .x, atypicalDevices = outliersIDs ) )
) %>%
  mutate_at( vars( contains("pValue") ), ~ round( .x, 5 ) )

outliersIDs <- c( "00:00:02:70", "00:00:02:85", "00:00:02:e2" )

if (plotAgain) {
  plotPaths <- tibble(
    material = materials
  ) %>%
    mutate(
      rainbow = map_chr( material, ~ rainbowPlot( wideMeasurements %>% filter( material == .x ), material= .x ) ),
      perDevice = map_chr( material, ~ individualCurves( wideMeasurements %>% filter( material == .x ), material= .x ) ),
      clusters = map_chr( material, ~ principalComponentsClustering( wideMeasurements %>% filter( material == .x ), material= .x ) ),
      )
  write_rds( plotPaths, "./graphics/plotPaths.rds" )
} else {
  plotPaths <- read_rds( "./graphics/plotPaths.rds" )
}


```


# Glossary

## Standards

These are objects which are especially selected due to having consistent phisical properties which allow us to know the values the scanner should be detecting and therefore serve as a reference.

### Main Standard

This standard is an in factory standard used for all of the devices.

### Local Standard

This is the standard provided to each user, togethed with the device. 


# General Performance

```{r general}

## identified as outliers in the clustering plos by principal outliers
pcOutliers <- c(
  "00:00:02:70",
  "00:00:02:85"
)

generalPrecission <- measurementsLong %>%
  filter( !( deviceID %in% pcOutliers ) ) %>%
  filter( !is.na(material) ) %>%
  group_by( material, wavelength ) %>%
  ## filter( !is.na(material) ) %>%
  summarise(
    relativeDev = sd( value, na.rm=TRUE ) / mean(value, na.rm=TRUE)
    ## relativeDev = mad( value, na.rm=TRUE ) / median(value, na.rm=TRUE)
  ) %>%
  pivot_wider( values_from = relativeDev, names_from = wavelength )

write_csv( generalPrecission, "./tables/generalPrecissionAcross.csv" )

wheatMaterials <- measurementsLong$material %>%
  unique %>%
  keep( str_detect(., "wheat") )
  
soilMaterials <- measurementsLong$material %>%
  unique %>%
  keep( str_detect(., "soil") )

oatsMaterials <- measurementsLong$material %>%
  unique %>%
  keep( str_detect(., "oat") )

generalPrecissionBetween <- measurementsLong %>%
  filter( !( deviceID %in% pcOutliers ) ) %>%
  filter( !is.na(material) ) %>%
  group_by( deviceID, wavelength, material ) %>%
  ## replace value by its median, eliminating internal variation
  summarise( deviceValue = median(value, na.rm=TRUE) ) %>%
  ungroup %>%
  mutate(
    material = ifelse( material %in% oatsMaterials, 'oats', material ),
    material = ifelse( material %in% wheatMaterials, 'wheat', material ),
    material = ifelse( material %in% soilMaterials, 'soil', material ),
  ) %>%
  group_by( material, wavelength ) %>%
  summarise(
    ## relativeDev = sd( deviceValue, na.rm=TRUE ) / mean( deviceValue, na.rm=TRUE),
    standardDeviation = sd( deviceValue, na.rm=TRUE ),
    mean = mean( deviceValue, na.rm=TRUE),
    relativeDev = standardDeviation / mean,
    interval = paste0( '(', round( mean - 2 * standardDeviation, 2 ), ',', round( mean + 2 * standardDeviation, 2 ), ')' )
  )

generalPrecissionBetweenMeans <- generalPrecissionBetween %>%
  select( mean, wavelength ) %>%
  pivot_wider( values_from = mean, names_from = wavelength )

generalPrecissionBetweenDev <- generalPrecissionBetween %>%
  select( relativeDev, wavelength ) %>%
  pivot_wider( values_from = relativeDev, names_from = wavelength )

generalPrecissionBetweenRanges <- generalPrecissionBetween %>%
  select( interval, wavelength ) %>%
  pivot_wider( values_from = interval, names_from = wavelength )

write_csv( generalPrecissionBetween, "./tables/generalPrecissionBetween.csv" )
write_csv( generalPrecissionBetweenMeans, "./tables/generalPrecissionBetweenMeans.csv" )
write_csv( generalPrecissionBetweenDev, "./tables/generalPrecissionBetweenDeviations.csv" )
write_csv( generalPrecissionBetweenRanges, "./tables/generalPrecissionBetweenRanges.csv" )

generalPrecissionInside <- measurementsLong %>%
  filter( !( deviceID %in% pcOutliers ) ) %>%
  filter( !is.na(material) ) %>%
  group_by( material, wavelength, deviceID ) %>%
  summarise(
    mean = mean(value, na.rm=TRUE),
    standardDeviation = sd( value, na.rm=TRUE ),
    relativeDev = standardDeviation / mean,
  ) %>%
  ungroup() %>%
  ## stop grouping by device, so a devices median is obtained
  mutate(
    material = ifelse( material %in% oatsMaterials, 'oats', material ),
    material = ifelse( material %in% wheatMaterials, 'wheat', material ),
    material = ifelse( material %in% soilMaterials, 'soil', material ),
    ) %>%
  group_by( material, wavelength ) %>%
  summarise(
    meanDev = mean( relativeDev ),
    mean = mean(mean),
    standardDeviation = mean(standardDeviation)
  ) %>%
  mutate(
    interval = paste0( '(', round( mean - 2 * standardDeviation, 2 ), ',', round( mean + 2 * standardDeviation, 2 ), ')' )
  )

generalPrecissionInsideMean <- generalPrecissionInside %>%
  select( mean, wavelength ) %>%
  pivot_wider( values_from = mean, names_from = wavelength )

generalPrecissionInsideDev <- generalPrecissionInside %>%
  select( meanDev, wavelength ) %>%
  pivot_wider( values_from = meanDev, names_from = wavelength )

generalPrecissionInsideRanges <- generalPrecissionInside %>%
  select( interval, wavelength ) %>%
  pivot_wider( values_from = interval, names_from = wavelength )

write_csv( generalPrecissionInside, "./tables/generalPrecissionInside.csv" )
write_csv( generalPrecissionInsideMean, "./tables/generalPrecissionInsideMean.csv" )
write_csv( generalPrecissionInsideDev, "./tables/generalPrecissionInsideDeviations.csv" )
write_csv( generalPrecissionInsideRanges, "./tables/generalPrecissionInsideRanges.csv" )


## measurementsLong %>%
##   filter( deviceID == "00:00:02:0b" ) %>%
##   group_by( material, wavelength ) %>%
##   summarise( relativeDev = sd( value, na.rm=TRUE ) / mean(value, na.rm=TRUE) ) %>%
##   pivot_wider( values_from = relativeDev, names_from = wavelength )

## oneAnova <- wideMeasurements %>%
##   filter( material == "standard" ) %>%
##   select( deviceID, "365" ) %>%
##   lm( `365`~deviceID, . )
## oneAnova %>% summary

```

#### Outlier Devices

* These devices can be observed in the plots, but haven't been considered for this measures, because they are considered out of the manufacturing standard.

```{r}

datatable( tibble("Outlier Device ID" = pcOutliers),
          caption ="Outlier Devices",
          )

```

# Variation Across and Inside Devices For Scan Targets { .tabset }


## Total

This is the relative standard deviation observed over all repetitions, irrespective of defice.

```{r}
## generalPrecission %>%
##   kable( caption = 'Variation for all Measures in All Devices', format = "html", digits = 3 )

datatable( generalPrecission %>% mutate_at( waveLengths, ~ round( x=.x, digits=3 ) ),
          extensions = "Buttons",
          caption ='Variation for all Measures in All Devices',
          options = list(
            dom ='Bfrtip',
            buttons = c('csv')
          )
)
```


## Intensity Values and Precission Across Devices

This is the relative standard deviation observed between the median curves for each device.

That is, we first forget all variation inside each device (changing the repeated measurements by the median value), then measure the variation across different devices.

### Intensity Means for Each Frequency on Each Target Material

```{r}
## generalPrecissionBetween %>%
##   kable( caption = 'Variation Between Device Medians', format = "html", digits = 3 )

datatable( generalPrecissionBetweenMeans %>% mutate_at( waveLengths, ~ round( x=.x, digits=3 ) ),
          extensions = "Buttons",
          caption = 'Device Means',
          options = list(
            dom ='Bfrtip',
            buttons = c('csv')
          )
          )
```

### Relative Deviation for Each Frequency on Each Target Material

```{r}
datatable( generalPrecissionBetweenDev %>% mutate_at( waveLengths, ~ round( x=.x, digits=3 ) ),
          extensions = "Buttons",
          caption = 'Relative Variation Between Device Medians',
          options = list(
            dom ='Bfrtip',
            buttons = c('csv')
          ),
          )

```

### Intervals Around the Mean (2sd)  for Each Frequency on Each Target Material

```{r}
datatable( generalPrecissionBetweenRanges,
          extensions = "Buttons",
          caption = 'Two standard deviations around the mean',
          options = list(
            dom ='Bfrtip',
            buttons = c('csv')
          ),
          )
```

## Inside Each Device

This is the variation calculated for the repetitions on each device, then averaged across devices. That is, the standard deviation is calculated first, by device. Then this internal SD is averaged.

### Intensity Means for Each Frequency on Each Target Material

```{r}
## generalPrecissionInside %>%
##   kable( caption = 'Variation Inside Device Medians', format = "html", digits = 3 )

datatable( generalPrecissionInsideMean %>% mutate_at( waveLengths, ~ round( x=.x, digits=3 ) ),
          extensions = "Buttons",
          caption = 'Device Means',
          )
```

### Relative Deviation for Each Frequency on Each Target Material

```{r}
datatable( generalPrecissionInsideDev %>% mutate_at( waveLengths, ~ round( x=.x, digits=3 ) ),
          extensions = "Buttons",
          caption = 'Relative Variation Inside Device Medians',
          options = list(
            dom ='Bfrtip',
            buttons = c('csv')
          ),
          )

```

### Intervals Around the Mean (2sd)  for Each Frequency on Each Target Material

```{r}
datatable( generalPrecissionInsideRanges,
          extensions = "Buttons",
          caption = 'Two standard deviations around the mean',
          options = list(
            dom ='Bfrtip',
            buttons = c('csv')
          ),
          )
```



# Plots for Scan Targets { .tabset }

The intention of this extensive plot coverage is to illustrate several relevant situations.

## Meaning and Intention of Each Plot

### How to see the plots?

Above this paragraph, tabs are available to select each material, and after the material new tabs allow to select each mentioned visualization.

### Rainbow Plots

 Rainbow Plots compare superimposed medians of the curves for each device. This illustrates the variation between devices (title _All Device Means_).
 
### Faceted Curves Plots

  The faceted plots titled _All Repetitions By Device_ allow to compare how variation happens for a unique device, but also comparing the behaviour across multiple devices, when the superimposed versions are too messy or too thight.
  
### Clusterization over Principal Components

  The _Clusterization Over Principal Components_ is an attempt to show with as much detail as possible a general panorama of the whole systemic behaviour in a unique plot.
  
  Each device had it's 10 wavelenghts transformed into it's 2 first principal components and each measurement has been plotted over that system, forming an area for each device.
  
* Small area for a given polygon implies the device informing the polygon is precise across repetitions.
* Small distance between the polygons (or even better, superimposition) implies the calibration for them is close.
* We've chosen not to normalize the scales, so even though transformed metrics as this should be evaluated with care, it can be said that the units are the same used for the original intensities.
  
  The desirable outcome is all areas should be both small and superimposed. For some materials, such as the barium sulfate *standard* this is arguably the case (even though the rigurous statistical testing systematically rejects the hypothesis of equal means, probably because the low sample count implies a lack in power). We also get to see partial coincidence for several of the natural objects, mainly because the areas are much bigger due to an increase on the intra device variance. 
  Some areas are rather tiny and if the readers follows up to the section on tests, in fact this is backed by the performer multivariate ANOVAs. 
  

```{r, echo=FALSE, results="asis"}

template <- "## %s { .tabset }

### All Device Means 

![RainbowPlot](%s)

### All Repetitions By Device

![FacetPlot](%s)

### Clusterization Over Principal Components

![Clusterization](%s)

"

pwalk( list( plotPaths$material, plotPaths$rainbow, plotPaths$perDevice, plotPaths$clusters ),
      ~ cat( sprintf( template, ..1, ..2, ..3, ..4 ) )
      )

```

# Multi Varied ANOVA Tests for Each Target 

In this section, we perform a two way multi varied ANOVA to test wether we can statistically assure the means are similar. 
Considering the low samples count, the test is exceedingly demanding but an intersting output anyway.

* These comparisons test the distribution of all the 10 wavelengths together.
* The factors are the `deviceID` (30 different) and `repetition` for each given device (1 to 6). 

Even considering the above mentioned conditions, we've been able to validate equality of means over repetitions for a good number of targets. The hypothesis of equality of means across devices is rejected with almost 0 p value in all cases. This can probably be addressed with higher sample counts. In all cases, the clustering algorithms for the first two principal components are a much better illustration of the situation. 

```{r anovaTable}
datatable( allMANOVATests ,
          extensions = "Buttons",
          caption ='Variation for all Measures in All Devices' ,
)
```

<!-- # Derivatives as Predictors -->

<!-- ```{r} -->

<!-- waveLengthsMatrix <- wideMeasurements %>% select( !!waveLengths ) %>% as.matrix -->

<!-- getDeltasMatrix <- function( cols ) { -->
<!--   map( 0:(cols-2), ~ c( rep(0,times = .x), -1,1, rep( 0,times = cols - 2 - .x ) ) ) %>% -->
<!--     c( ., -->
<!--       c( 1, rep( x=0, times=cols-2), -1 ) -->
<!--       ) %>% -->
<!--     unlist %>% -->
<!--     matrix( data = .,  ncol = cols ) -->
<!-- } -->

<!-- deltasMatrix <- getDeltasMatrix( 10 ) -->
<!-- deltasMatrix -->

<!-- deltasTable <- waveLengthsMatrix %*% deltasMatrix -->

<!-- deltaVars <- map_chr( 1:10, ~ paste0( "delta-", .x ) ) -->

<!-- colnames(deltasTable) <- deltaVars  -->

<!-- deltasTable <- as_tibble( deltasTable ) %>% -->
<!--   bind_cols( wideMeasurements %>% select( deviceID, repetition, material ), . ) -->

<!-- applyMANOVAtoDeltas <- function( materialName, atypicalDevices = c() ) { -->
<!--   data <- deltasTable %>% -->
<!--     filter( material == materialName, !( deviceID %in% atypicalDevices ) ) -->

<!--   test <- manova( cbind( `delta-1`, `delta-2`,`delta-3`,`delta-4`,`delta-5`,`delta-6`,`delta-7`,`delta-8`,`delta-9` ) ~ deviceID, data ) %>% -->
<!--     summary -->
<!--   pValue <- test$stats %>% as_tibble %>% select( `Pr(>F)` ) %>% .[[1,1]] -->
<!--   return(pValue) -->
<!-- } -->

<!-- wide2 <- wideMeasurements %>% -->
<!--   mutate( across( !!waveLengths, ~ .x - .data[["450"]] ) ) -->

<!-- rainbowPlot( data = wide2 %>% filter(material == "standard") , material = "standard" ) -->


<!-- flatStandardDevs <- c( -->
<!--   "00:00:02:77", -->
<!--   "00:00:02:79", -->
<!--   "00:00:02:7a", -->
<!--   "00:00:02:7d", -->
<!--   "00:00:02:c7", -->
<!--   "00:00:02:d9", -->
<!--   "00:00:02:f8" -->
<!-- ) -->

<!-- noFlatDevs <- allDevices %>% keep( !( . %in% flatStandardDevs ) ) -->

<!-- allMANOVATests2 <- tibble( -->
<!--   material = materials, -->
<!--   map_df( materials, ~ applyMANOVAReps( materialName = .x, atypicalDevices = noFlatDevs ) ) -->
<!-- ) %>% -->
<!--   mutate_at( vars( contains("pValue") ), ~ round( .x, 5 ) ) -->

```
